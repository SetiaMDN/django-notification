import os
import traceback
from twilio.rest import Client
from .models import NotificationUser
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.shortcuts import render, redirect
from pathlib import Path
from django.http import HttpResponse

BASE_DIR = Path(__file__).resolve().parent.parent

account_sid = "account_sid"
auth_token  = "auth_token"

from_whatsapp_number='whatsapp:+from_whatsapp_number'

client = Client(account_sid, auth_token)

def send_email_wa(user, feature, message, media):
    server = None
    try:
        to_whatsapp_number = 'whatsapp:+to_whatsapp_number'
        sender_email = "sender_email"

        merge_data = {
            'product_class': "BisaKita",
            'product_name': feature,
            'name': message
        }

        subject = render_to_string(os.path.join(BASE_DIR, "notification/templates/email_subject.txt"), merge_data).strip()
        text_body = render_to_string(os.path.join(BASE_DIR, "notification/templates/email_body.txt"), merge_data)
        
        if media == "email":
            msg = EmailMultiAlternatives(subject=subject, from_email=sender_email, to=[user], body=text_body)
            msg.send()
        elif media == "whatsapp":
            client.messages.create(body = subject + "\n\n" + text_body, from_ = from_whatsapp_number, to = to_whatsapp_number)

    except Exception:
        traceback.print_exc()
        print("Failed to send email to %s." %(user))

    finally:
        if server:
            server.quit()

@csrf_exempt
def broadcast_email(request):
    # Tipe notifikasi broadcast
    feature = request.POST["feature"]
    message = request.POST["message"]

    # Cek daftar user
    user_list = list(NotificationUser.objects.all())

    print(feature)
    # # kirim email
    for user in user_list:
        if feature in user.enabledNotifications["data"].split(","):
            if "email" in user.enabledNotifications["data"].split(","):
                send_email_wa(user.email, feature, message, "email")
            if "whatsapp" in user.enabledNotifications["data"].split(","):
                send_email_wa(user.email, feature, message, "whatsapp")
    return HttpResponse('pesan sudah diproses')

@csrf_exempt
def notify(request):
    # Tipe notifikasi private
    email = request.POST["email"]
    feature = request.POST["feature"]
    message = request.POST["message"]

    # Cek daftar user
    user = NotificationUser.objects.get(email=email)
    print(feature)
    print(user)
    print(user.enabledNotifications["data"].split(","))
    if feature not in user.enabledNotifications["data"].split(","):
        return HttpResponse('pesan tidak dikirim karena user tidak subscribe')

    # kirim email
    if "email" in user.enabledNotifications["data"].split(","):
        send_email_wa(user.email, feature, message, "email")
    if "whatsapp" in user.enabledNotifications["data"].split(","):
        send_email_wa(user.email, feature, message, "whatsapp")
    return HttpResponse('pesan dikirim')

def notificationSettingsView(request, email):
    model = NotificationUser
    notification_profile = model.objects.get(email=email)
    if request.method == "GET":
        return render(request, os.path.join(BASE_DIR, "notification/page/notification_settings.html"), {'notification_profile': notification_profile})
    elif request.method == "POST":
        feature = request.POST["enabledFeatures"]
        print(feature)
        notification_profile.enabledNotifications["data"] = feature
        notification_profile.save()
        return redirect("notification_settings", email)