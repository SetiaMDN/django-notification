from django.db import models

# Create your models here.

class NotificationUser(models.Model):
    email = models.CharField(max_length=2000)
    enabledNotifications = models.JSONField()